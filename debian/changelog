jruby-maven-plugins (3.0.1-1) unstable; urgency=medium

  * New upstream version 3.0.1
  * removed poms from new upstream version
  * d/maven.rules: use new package namespace
  * add gem-maven-plugin relocation for version 3.0.0
  * ignore unneeded maven deps
  * add d/docs to ship upstream README
  * d/copyright: update copyright infos
  * d/control:
    + rename binary package to conform to policy
    + add myself to Uploaders
    + build using headless jdk
    + update Vcs-* and Homepage URLs
    + add Rules-Require-Root: no
    + bump Standards-Version to 4.6.2, no change needed
    + run wrap-and-sort -bastk
  * d/patches:
    + rebase patches for new upstream version
    + fix dep-3 patch metadata
    + add new patch to fix jruby execution
  * add lintian override for javalib-but-no-public-jars
  * do not embed maven-tools gem or jruby-complete
  * d/rules: cleanup unnecessary rules

 -- Jérôme Charaoui <jerome@riseup.net>  Sat, 21 Oct 2023 17:40:58 -0400

jruby-maven-plugins (1.1.5+ds1-2) unstable; urgency=medium

  * Team upload.
  * Fixed a flawed assertion that fail to compile with the source level 1.7
    (Closes: #867648)
  * Removed plexus-archiver.patch and depend on libplexus-archiver-java (>= 3.5)
  * Fixed the execution of install_gems during the build
  * Depend on libplexus-component-annotations-java instead of
    libplexus-containers-java
  * Standards-Version updated to 4.1.0

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 05 Sep 2017 23:45:37 +0200

jruby-maven-plugins (1.1.5+ds1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.1.5+ds1.
  * Add libyaml-snake-java to Build-Depends.
  * orig-tar.sh: Use +ds instead of .ds.
  * Switch to compat level 10.
  * Declare compliance with Debian Policy 3.9.8.
  * Vcs-fields: Use https and cgit.
  * Add snakeyaml.patch fix FTBFS with SnakeYaml 1.17. (Closes: #844841)
  * Add plexus-archiver.patch and fix another FTBFS since Debian still uses
    version 2 of plexus archiver.
  * Skip the tests.

 -- Markus Koschany <apo@debian.org>  Wed, 28 Dec 2016 21:07:46 +0100

jruby-maven-plugins (1.0.10.ds1-2) unstable; urgency=medium

  * Team upload.
  * Build with maven-debian-helper 2

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 21 Dec 2015 16:55:55 +0100

jruby-maven-plugins (1.0.10.ds1-1) unstable; urgency=medium

  * Initial release. (Closes: #796002).

 -- Miguel Landaeta <nomadium@debian.org>  Sat, 10 Oct 2015 22:52:50 -0300
